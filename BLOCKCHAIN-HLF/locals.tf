locals {
  common_tags = {
    ManagedBy = "Terraform"
    Module    = "BLOCKCHAIN-HLF"
    Env       = var.env
    Name      = var.name
    Project   = var.project
    Owner     = var.owner
    Customer  = var.customer
    Backup    = "Yes"
  }
}