Esse módulo cria uma EC2 para ser usada no blocjchain goledger, com HD secundário, IP Elástico e o security group especifico para deploy do hyperledger

Para usar esse módulo copie o bloco abaixo e altere os campos: 
module, ami, instance_type, name, root_hd_size, data_hd_size, project, owner, customer e env.


 module  "blockchain_rastreabilidade_hmlg" {

  source         = "git::https://gitlab.com/terraform-modules-for-aws/aws-terraform-modules.git//BLOCKCHAIN-HLF"

  security_group = [aws_security_group.infra_ports.id, aws_security_group.hlf-pre-prod.id]

  ssh_key        = aws_key_pair.ssh.id

  subnet_shared  = var.subnet_shared

  aws_zone       = var.regiao_aws

  ami            = "ami-0d4c664d2c7345cf1"

  instance_type  = "t2.small"

  name           = "srv-hlf-hmlg"

  root_hd_size   = "30"

  data_hd_size   = 1

  project        = "Rastreabilidade Swift"

  owner          = "Rodrigo Grapeia"

  customer       = "Swift"

  env            = "Pre-Prod"

}
