output "ec2-name" {
  value = aws_instance.this.tags.Name
}

output "ec2-private-ip" {
  value = aws_instance.this.private_ip
}