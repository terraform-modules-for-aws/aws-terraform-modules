variable "name" {
  description = "Nome da instância"
  type        = string
}

variable "project" {
  description = "Informe a qual projeto esse recurso pertence"
  type        = string
}

variable "owner" {
  description = "Informe o nome da pessoa responsavel por esse projeto"
  type        = string
}

variable "env" {
  description = "Informe o ambiente (Prod, Pre-Prod, HMLG our INFRA)"
  type        = string
}

variable "instance_type" {
  description = "Tamanho da instâcia (t2.micro/t2.medium/etc...)"
  type        = string
}

variable "security_group" {
  description = "SG Default Infra"
}

variable "subnet_shared" {
  description = "Subnet compartilhada"
  type        = string
}

variable "ssh_key" {
  description = "Chave SSH Padrão"
}

variable "aws_zone" {
  description = "Zona de diponibilidade"
}

variable "root_hd_size" {
  description = "Tamanho do HD"
}

variable "ami" {
  description = "Defina a imagem ami a ser utilizada"
}

variable "getpublicip" {
  description = "write 'true' or 'false'"
  default     = false
}