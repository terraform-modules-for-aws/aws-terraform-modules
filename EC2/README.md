Esse módulo cria uma EC2 padrão que pode ou não ter um IP externo dependendo do valor no campo getpublicip.

Para usar esse módulo copie o bloco abaixo e altere os campos: 
module, ami, instance_type, name, root_hd_size, project, owner, env e getpublicip.

module "EC2_01" {

  source         = "git::https://gitlab.com/terraform-modules-for-aws/aws-terraform-modules.git//EC2"

  security_group = [aws_security_group.infra_ports.id]

  ssh_key        = aws_key_pair.ssh.id

  subnet_shared  = var.subnet_shared

  aws_zone       = var.zone

  instance_type  = "t2.nano"

  name           = "srv-empresa-server"

  root_hd_size   = "8"

  ami            = var.ubuntu

  project        = "Sem projeto"

  owner          = "Kblo"

  env            = "Infra"

  getpublicip    = "true"

}
