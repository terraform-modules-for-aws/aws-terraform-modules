locals {
  common_tags = {
    ManagedBy = "Terraform"
    Module    = "EC2"
    Env       = var.env
    Name      = var.name
    Project   = var.project
    Owner     = var.owner
    Backup    = "Yes"
  }
}