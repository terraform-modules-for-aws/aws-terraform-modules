Esse módulo cria uma EC2 com IP elástico.

Para usar esse módulo copie o bloco abaixo e altere os campos: 
module, ami, instance_type, name, root_hd_size, project, owner e env.


module "EC2_01" {

  source         = "git::https://gitlab.com/terraform-modules-for-aws/aws-terraform-modules.git//EC2-ELASTIC-IP"

  security_group = [aws_security_group.infra_ports.id]

  ssh_key        = aws_key_pair.ssh.id

  subnet_shared  = var.subnet_shared

  aws_zone       = var.zone

  instance_type  = "t2.nano"

  name           = "srv-servidor"

  root_hd_size   = "8"

  ami            = var.ubuntu

  project        = "Sem projeto"

  owner          = "Kblo"

  env            = "Infra"

}
