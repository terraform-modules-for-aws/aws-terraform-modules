output "ec2-name" {
  value = aws_instance.this.tags.Name
}

output "ec2-private-ip" {
  value = aws_instance.this.private_ip
}

output "ec2-elastic-ip" {
  value = aws_eip_association.elastic_ip_association.public_ip
}
