resource "aws_eip_association" "elastic_ip_association" {
  instance_id   = aws_instance.this.id
  allocation_id = aws_eip.elastic_ip.id
}

resource "aws_instance" "this" {
  ami                    = var.ami
  instance_type          = var.instance_type
  subnet_id              = var.subnet_shared
  vpc_security_group_ids = var.security_group
  key_name               = var.ssh_key

  root_block_device {
    volume_size = var.root_hd_size
    volume_type = "gp2"
    tags        = local.common_tags
  }

  tags = local.common_tags
}

resource "aws_eip" "elastic_ip" {
  vpc  = true
  tags = local.common_tags
}